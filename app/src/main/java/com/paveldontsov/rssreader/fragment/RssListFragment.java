package com.paveldontsov.rssreader.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.paveldontsov.rssreader.R;
import com.paveldontsov.rssreader.activity.RssDetailActivity;
import com.paveldontsov.rssreader.adapter.RssAdapter;
import com.paveldontsov.rssreader.data.DataManager;
import com.prof.rssparser.Article;
import com.prof.rssparser.Parser;

import java.util.ArrayList;

import info.hoang8f.android.segmented.SegmentedGroup;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class RssListFragment extends Fragment {

    private static final String TAG = RssListFragment.class.getSimpleName();

    private static final int RSS_UPDATE_DELAY = 5000;
    private static final int MIN_PROGRESS_ANIM_DELAY = 1000;

    public static RssListFragment newInstance() {
        return new RssListFragment();
    }

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    private Handler handler;
    private Runnable loadRssRunnable;
    private RssAdapter rssAdapter;
    private CompositeDisposable compositeDisposable;

    private boolean loadFirst;

    //todo test without internet, add processing
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        loadRssRunnable = new Runnable() {
            @Override
            public void run() {
                loadRss();
                handler.postDelayed(this, RSS_UPDATE_DELAY);
            }
        };
        rssAdapter = new RssAdapter();
        rssAdapter.setRssItemClickListener((Article article) -> {
            DataManager.get().saveLastRssTopic(article.getTitle());
            startActivity(RssDetailActivity.newIntent(getActivity(), article));
        });
        compositeDisposable = new CompositeDisposable();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_rss_list, container, false);

        recyclerView = root.findViewById(R.id.frag_rss_list_recyclerview);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(rssAdapter);

        progressBar = root.findViewById(R.id.frag_rss_list_progress_bar);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.GONE);

        loadFirst = DataManager.get().getLastRssChanelIsFirst();

        SegmentedGroup segmentedGroup = root.findViewById(R.id.frag_rss_list_segment_group);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            segmentedGroup.setOrientation(LinearLayout.HORIZONTAL);
        } else {
            segmentedGroup.setOrientation(LinearLayout.VERTICAL);
        }
        segmentedGroup.updateBackground();

        if (loadFirst) {
            segmentedGroup.check(R.id.frag_rss_list_first_segment_rbtn);
        } else {
            segmentedGroup.check(R.id.frag_rss_list_sec_segment_rbtn);
        }
        segmentedGroup.setOnCheckedChangeListener((RadioGroup radioGroup, int id) -> {
            switch (id) {
                case R.id.frag_rss_list_first_segment_rbtn:
                    loadFirst = true;
                    break;
                case R.id.frag_rss_list_sec_segment_rbtn:
                    loadFirst = false;
                    break;
            }
            handler.removeCallbacks(loadRssRunnable);
            handler.postDelayed(loadRssRunnable, RSS_UPDATE_DELAY);
            loadRss();
            recyclerView.scrollToPosition(0);
        });


        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.post(loadRssRunnable);
    }

    @Override
    public void onPause() {
        super.onPause();
        DataManager.get().saveLastRssChanelIsFirst(loadFirst);
        handler.removeCallbacks(loadRssRunnable);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    private void loadRss() {
        if (loadFirst) {
            loadFirstSegment();
        } else {
            loadSecondSegment();
        }
    }

    private void loadFirstSegment() {
        long start = startProgress();
        compositeDisposable.add(rssObservable("http://feeds.reuters.com/reuters/businessNews")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(rssList -> {
                    rssAdapter.setModels(rssList);
                    rssAdapter.notifyDataSetChanged();
                    stopProgress(start);
                }, error -> {
                    Toast.makeText(getActivity(), R.string.rss_load_error, Toast.LENGTH_SHORT).show();
                }));
    }

    private void loadSecondSegment() {
        long start = startProgress();
        compositeDisposable.add(
                Observable.zip(
                        rssObservable("http://feeds.reuters.com/reuters/entertainment"),
                        rssObservable("http://feeds.reuters.com/reuters/environment"),
                        (ArrayList<Article> entertainment, ArrayList<Article> environment) -> {
                            entertainment.addAll(environment);
                            return entertainment;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(rssList -> {
                            rssAdapter.setModels(rssList);
                            rssAdapter.notifyDataSetChanged();
                            stopProgress(start);
                        }, error -> {
                            Toast.makeText(getActivity(), R.string.rss_load_error, Toast.LENGTH_SHORT).show();
                        }));
    }

    private long startProgress() {
        progressBar.setVisibility(View.VISIBLE);
        return System.currentTimeMillis();
    }

    private void stopProgress(long start) {
        long diff = System.currentTimeMillis() - start;
        if (diff < MIN_PROGRESS_ANIM_DELAY) {
            handler.postDelayed(() -> {
                progressBar.setVisibility(View.GONE);
            }, MIN_PROGRESS_ANIM_DELAY - diff);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private Observable<ArrayList<Article>> rssObservable(String httpSource) {
        return Observable.create(emitter -> {
            Parser parser = new Parser();
            parser.execute(httpSource);
            parser.onFinish(new Parser.OnTaskCompleted() {
                @Override
                public void onTaskCompleted(ArrayList<Article> list) {
                    emitter.onNext(list);
                    emitter.onComplete();
                }

                @Override
                public void onError() {
                    emitter.tryOnError(new Exception("Cannot load rss"));
                }
            });
        });
    }
}
