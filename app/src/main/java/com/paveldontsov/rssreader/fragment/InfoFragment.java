package com.paveldontsov.rssreader.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.paveldontsov.rssreader.R;
import com.paveldontsov.rssreader.data.DataManager;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InfoFragment extends Fragment {

    public static InfoFragment newInstance() {
        return new InfoFragment();
    }

    private TextView dateTimeTv;
    private TextView labelTv;

    private Handler handler;
    private Runnable dateTimeUpdateRunnable;
    private SimpleDateFormat dateFormat;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        dateFormat = new SimpleDateFormat("dd.MM.yyy HH:mm:ss");
        dateTimeUpdateRunnable = new Runnable() {

            @Override
            public void run() {
                dateTimeTv.setText(dateFormat.format(new Date()));
                handler.postDelayed(this, 1000);
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_info, container, false);

        dateTimeTv = root.findViewById(R.id.frag_info_date_time_tv);
        labelTv = root.findViewById(R.id.frag_info_label_tv);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        handler.post(dateTimeUpdateRunnable);
        labelTv.setText(DataManager.get().getLastRssTopic());
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(dateTimeUpdateRunnable);
    }
}
