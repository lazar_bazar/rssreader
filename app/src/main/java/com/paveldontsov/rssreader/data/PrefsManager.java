package com.paveldontsov.rssreader.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefsManager {

    private SharedPreferences prefs;

    public PrefsManager(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void save(String key, String val) {
        prefs.edit()
                .putString(key, val)
                .apply();
    }

    public String getString(String key) {
        return prefs.getString(key, null);
    }

    public String getString(String key, String defVal) {
        return prefs.getString(key, defVal);
    }

    public void save(String key, int val) {
        prefs.edit()
                .putInt(key, val)
                .apply();
    }

    public int getInt(String key) {
        return prefs.getInt(key, 0);
    }

    public void save(String key, boolean val) {
        prefs.edit()
                .putBoolean(key, val)
                .apply();
    }

    public boolean getBool(String key) {
        return prefs.getBoolean(key, false);
    }

    public boolean getBool(String key, boolean defVal) {
        return prefs.getBoolean(key, defVal);
    }
}
