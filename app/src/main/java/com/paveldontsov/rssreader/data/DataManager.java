package com.paveldontsov.rssreader.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.paveldontsov.rssreader.App;
import com.paveldontsov.rssreader.activity.MainActivity;

public class DataManager {

    private static final String LAST_VIEW_PAGER_POS_KEY = "last_fragment";
    private static final String LAST_RSS_CHANEL_IS_FIRST_KEY = "last_rss_chanel_is_first";
    private static final String LAST_RSS_TOPIC_KEY = "last_rss_topic";

    private static DataManager instance;

    private PrefsManager prefsManager;

    private DataManager(Context context) {
        prefsManager = new PrefsManager(context);
    }

    public static DataManager get() {
        if (instance == null) {
            instance = new DataManager(App.get());
        }
        return instance;
    }

    public void saveLastViewPagerPos(int pos) {
        prefsManager.save(LAST_VIEW_PAGER_POS_KEY, pos);
    }

    public int getLastViewPagerPos() {
        return prefsManager.getInt(LAST_VIEW_PAGER_POS_KEY);
    }

    public void saveLastRssChanelIsFirst(boolean val) {
        prefsManager.save(LAST_RSS_CHANEL_IS_FIRST_KEY, val);
    }

    public boolean getLastRssChanelIsFirst() {
        return prefsManager.getBool(LAST_RSS_CHANEL_IS_FIRST_KEY, true);
    }

    public void saveLastRssTopic(String topic) {
        prefsManager.save(LAST_RSS_TOPIC_KEY, topic);
    }

    public String getLastRssTopic() {
        return prefsManager.getString(LAST_RSS_TOPIC_KEY, "");
    }
}
