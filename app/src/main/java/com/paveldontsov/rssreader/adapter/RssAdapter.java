package com.paveldontsov.rssreader.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.paveldontsov.rssreader.R;
import com.prof.rssparser.Article;

import java.util.ArrayList;
import java.util.List;

public class RssAdapter extends RecyclerView.Adapter<RssAdapter.RssViewHolder> {

    public interface OnRssItemClickListener {

        void onClick(Article article);
    }

    private OnRssItemClickListener rssItemClickListener;
    private List<Article> models;
    private LayoutInflater inflater;

    public RssAdapter() {
        models = new ArrayList<>();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        inflater = LayoutInflater.from(recyclerView.getContext());
    }

    @NonNull
    @Override
    public RssViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {
        View itemView = inflater.inflate(R.layout.rss_item, viewGroup, false);
        return new RssViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RssViewHolder rssViewHolder, int pos) {
        rssViewHolder.bind(models.get(pos));
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void setModels(List<Article> models) {
        this.models = models;
    }

    public List<Article> getModels() {
        return models;
    }

    public void setRssItemClickListener(OnRssItemClickListener rssItemClickListener) {
        this.rssItemClickListener = rssItemClickListener;
    }

    public class RssViewHolder extends RecyclerView.ViewHolder {

        private TextView dateTv;
        private TextView titleTv;

        private Article model;

        public RssViewHolder(@NonNull View itemView) {
            super(itemView);

            dateTv = itemView.findViewById(R.id.rss_item_date_tv);
            titleTv = itemView.findViewById(R.id.rss_item_title_tv);
            itemView.setOnClickListener(view -> {
                if (rssItemClickListener != null) {
                    rssItemClickListener.onClick(model);
                }
            });
        }

        public void bind(Article model) {
            this.model = model;
            CharSequence dateText = DateFormat.format("dd.MM.yyyy\t\thh:mm", model.getPubDate());
            dateTv.setText(dateText);
            titleTv.setText(model.getTitle());
        }
    }
}
