package com.paveldontsov.rssreader.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;

import com.paveldontsov.rssreader.R;
import com.prof.rssparser.Article;

import org.jsoup.Jsoup;

import java.util.Date;

public class RssDetailActivity extends AppCompatActivity {

    private static final String TAG = RssDetailActivity.class.getSimpleName();

    private static final String TITLE_ARG = "title";
    private static final String DESC_ARG = "desc";
    private static final String DATE_ARG = "date";
    private static final String BUNDLE_ARG = "bundle";
    private static final String DATE_STR_ARG = "date_str";

    public static Intent newIntent(Context context, Article article) {
        Intent intent = new Intent(context, RssDetailActivity.class);

        Bundle args = new Bundle();
        args.putString(TITLE_ARG, article.getTitle());
        String formatDesc = Jsoup.parse(article.getDescription()).text();
        args.putString(DESC_ARG, formatDesc);
        args.putSerializable(DATE_ARG, article.getPubDate());

        intent.putExtra(BUNDLE_ARG, args);

        return intent;
    }

    private TextView titleTv;
    private TextView dateTv;
    private TextView descTv;

    private String titleStr;
    private String dateStr;
    private String descStr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_rss_detail);

        if (savedInstanceState != null) {
            Bundle args = savedInstanceState.getBundle(BUNDLE_ARG);
            titleStr = args.getString(TITLE_ARG);
            dateStr = args.getString(DATE_STR_ARG);
            descStr = args.getString(DESC_ARG);
        } else {
            Intent intent = getIntent();
            Bundle args = intent.getBundleExtra(BUNDLE_ARG);
            titleStr = args.getString(TITLE_ARG);
            Date date = (Date) args.getSerializable(DATE_ARG);
            dateStr = DateFormat.format("dd.MM.yyyy\t\thh:mm", date).toString();
            descStr = args.getString(DESC_ARG);
        }

        Log.d(TAG, "title: " + titleStr);
        Log.d(TAG, "date: " + dateStr);
        Log.d(TAG, "desc: " + descStr);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(view -> {
            finish();
        });
        getSupportActionBar().setTitle(R.string.rss_detail);

        titleTv = findViewById(R.id.frag_rss_detail_title_tv);
        titleTv.setText(titleStr);
        dateTv = findViewById(R.id.frag_rss_detail_date_tv);
        dateTv.setText(dateStr);
        descTv = findViewById(R.id.frag_rss_detail_desc_tv);
        descTv.setText(descStr);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle bundle = new Bundle();
        bundle.putString(TITLE_ARG, titleStr);
        bundle.putString(DESC_ARG, descStr);
        bundle.putString(DATE_STR_ARG, dateStr);
        outState.putBundle(BUNDLE_ARG, bundle);
    }
}
