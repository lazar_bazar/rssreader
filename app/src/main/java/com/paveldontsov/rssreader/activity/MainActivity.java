package com.paveldontsov.rssreader.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.paveldontsov.rssreader.R;
import com.paveldontsov.rssreader.data.DataManager;
import com.paveldontsov.rssreader.fragment.InfoFragment;
import com.paveldontsov.rssreader.fragment.RssListFragment;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Toolbar toolbar;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = findViewById(R.id.act_main_tablayout);
        ViewPager container = findViewById(R.id.act_main_frag_container);
        tabLayout.setupWithViewPager(container);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(container));
        FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int pos) {
                switch (pos) {
                    case 0:
                        return InfoFragment.newInstance();
                    case 1:
                        return RssListFragment.newInstance();
                }
                return null;
            }

            @Override
            public int getCount() {
                return 2;
            }
        };
        container.setAdapter(adapter);

        tabLayout.getTabAt(0).setText(R.string.info);
        tabLayout.getTabAt(1).setText(R.string.rss);

        setupFragment();
    }

    @Override
    protected void onPause() {
        super.onPause();
        DataManager.get().saveLastViewPagerPos(tabLayout.getSelectedTabPosition());
    }

    private void setupFragment() {
        int lastPos = DataManager.get().getLastViewPagerPos();
        if (tabLayout.getSelectedTabPosition() == lastPos) {
            return;
        }
        tabLayout.getTabAt(lastPos).select();
    }
}
